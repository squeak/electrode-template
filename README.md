
# electrode-template

The electrode template is an app that you can download and use out of the box.
It presents how an [electrode app](https://squeak.eauchat.org/libs/electrode/) should be organized and let you start coding your app's pages without having to learn all about electrode to begin with.
Just use it to make your app, and start learning on the fly :)

## Install

Clone this directory wherever you want on your system. Then run:
```bash
cd /path/to/where-you-put/the-template-app/
npm install
```
This will install the necessary dependencies in `node_modules`.

Then you can start the app with `npm start` and visit your browser at `localhost:9876` or whichever other port you chose in `boot/config.js`.


## Now you can start tweaking to make your own app :)

You can for example:
  - modify the content of the homepage by modifying `pages/index/index.js` and `pages/index/index.less`,
  - add new pages:
    * add a new object in the `pages` array in `routes/pages.js`,
    * and add the script and style files for this page in `pages`, create `pages/<pageName>/<pageName>.js` and `pages/<pageName>/<pageName>.less`,
  - modify the configuration of the app (in `boot/config.js`), for example change the port at which the app is served, or add authentication for some pages.

For more info and documentation, visit [the electrode wiki](https://squeak.eauchat.org/libs/electrode/?documentation).
